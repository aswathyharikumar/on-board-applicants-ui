package com.amex.demo.applicantonboardingui.service;

import com.amex.demo.applicantonboardingui.model.Applicant;
import org.springframework.http.HttpStatus;

import java.util.List;

public interface UIService{

List<Applicant> getApplicants(String applicantStatus);
Applicant getApplicantById(int id);
Applicant createApplicant(Applicant applicant);
Applicant updateApplicant(int id, Applicant applicant);

}