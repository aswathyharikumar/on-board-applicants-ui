package com.amex.demo.applicantonboardingui.service;

import com.amex.demo.applicantonboardingui.model.Applicant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class UIServiceImpl implements UIService {

    Logger logger = LoggerFactory.getLogger(UIServiceImpl.class);

    @Autowired
    RestTemplate restTemplate;

    final String ROOT_URI = "http://localhost:8080/onboard/applicants";

    @Override
    public List<Applicant> getApplicants(String applicantStatus) {
        logger.info("Inside UIServiceImpl::getApplicants::" + applicantStatus);
        ResponseEntity<List<Applicant>> responseEntity = null;

        try {
                responseEntity = restTemplate.exchange(applicantStatus == null ? ROOT_URI : ROOT_URI + "?status=" + applicantStatus, HttpMethod.GET, null, new ParameterizedTypeReference<List<Applicant>>(){});

        } catch (RestClientException e) {
                logger.error("Caught::Exception while calling the REST API");
        }
        if(responseEntity != null){
            logger.info("Response is {}", responseEntity.getBody());
        }
        return responseEntity == null ? null : responseEntity.getBody();
    }


    @Override
    public Applicant getApplicantById(int id) {
        ResponseEntity<Applicant> responseEntity = restTemplate.getForEntity(ROOT_URI + "/" + id, Applicant.class);
        return responseEntity.getBody();
    }

    @Override
    public Applicant createApplicant(Applicant applicant) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<Applicant> request = new HttpEntity<>(applicant, headers);
        ResponseEntity<Applicant> responseEntity = restTemplate.exchange(ROOT_URI, HttpMethod.POST, request, Applicant.class);

        return responseEntity.getBody();
    }

    @Override
    public Applicant updateApplicant(int id, Applicant applicant) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<Applicant> request = new HttpEntity<>(applicant, headers);
        restTemplate.exchange(ROOT_URI + "/" + id, HttpMethod.PUT, request, Void.class);
        Applicant updatedApplicant = getApplicantById(id);

        return applicant;
    }
}
