package com.amex.demo.applicantonboardingui.service;

import com.amex.demo.applicantonboardingui.model.Role;
import com.amex.demo.applicantonboardingui.model.User;
import com.amex.demo.applicantonboardingui.repository.UsersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {

    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> user = usersRepository.findByFirstName(username);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        if(user.isPresent()){
            for (Role role : user.get().getRoles()){
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
            }
            return new org.springframework.security.core.userdetails.User(user.get().getFirstName(), user.get().getPassword(), grantedAuthorities);
        }else{
            throw new UsernameNotFoundException("User name does not exist");
        }

    }
}
