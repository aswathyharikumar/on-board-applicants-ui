package com.amex.demo.applicantonboardingui.ui;

import com.amex.demo.applicantonboardingui.service.UIService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

@Route("")
@PageTitle("Applicant Eligibility Checker")
public class ApplicantView extends VerticalLayout {

    private ApplicantForm applicantForm ;
    private UIService uiService;


    public ApplicantView(ApplicantForm applicantForm, UIService uiService){
        this.applicantForm=applicantForm;
        this.uiService=uiService;
        Button adminLoginButton = new Button("Administrator", new Icon(VaadinIcon.ARROW_RIGHT));
        adminLoginButton.addClickListener(event -> {
            //redirect to login page
            getUI().ifPresent(ui -> ui.navigate("login"));
        });
        Label pageLabel = new Label("Applicant Eligibility Checker");
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(pageLabel, adminLoginButton);
        add(horizontalLayout, applicantForm);

    }

}
