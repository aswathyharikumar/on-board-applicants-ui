package com.amex.demo.applicantonboardingui.ui;

import com.amex.demo.applicantonboardingui.model.Address;
import com.amex.demo.applicantonboardingui.model.Applicant;
import com.amex.demo.applicantonboardingui.service.UIService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.BindingValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.function.SerializablePredicate;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringComponent
@UIScope
public class ApplicantForm extends FormLayout {

    Logger logger = LoggerFactory.getLogger(ApplicantForm.class);

    private TextField firstName = new TextField();
    private TextField lastName = new TextField();
    private TextField mothersMaidenName = new TextField();
    private DatePicker dateOfBirth = new DatePicker();
    private ListBox<String> jobStatus = new ListBox<String>();
    private ListBox<String> residentialStatus = new ListBox<String>();
    private ListBox<String> title = new ListBox<String>();

    private TextField jobDescription = new TextField();
    private TextField annualIncome = new TextField();
    private TextField emailAddress = new TextField();
    private TextField phoneNumber = new TextField();
    private TextField buildingNumber = new TextField();
    private TextField streetName = new TextField();
    private TextField postCode = new TextField();
    private Button save = new Button("Save");
    private Button reset = new Button("Reset");
    private Applicant applicant;
    private Address address;
    private final UIService uiService;


    Binder<Applicant> binder = new Binder<>(Applicant.class);
    Binder<Address> binderAddress = new Binder<>(Address.class);

    public ApplicantForm(UIService uiService){

        this.uiService = uiService;
        FormLayout applicantFormLayout = new FormLayout();
        applicantFormLayout.addFormItem(title,"Title");
        applicantFormLayout.addFormItem(firstName,"First Name");
        applicantFormLayout.addFormItem(lastName,"Last Name");
        applicantFormLayout.addFormItem(mothersMaidenName,"Mothers Maiden Name");
        applicantFormLayout.addFormItem(dateOfBirth,"Date of Birth");
        applicantFormLayout.addFormItem(emailAddress,"Email Address");
        applicantFormLayout.addFormItem(phoneNumber,"Phone Number");
        applicantFormLayout.addFormItem(jobStatus,"Job Status");
        applicantFormLayout.addFormItem(jobDescription,"Job Description");
        applicantFormLayout.addFormItem(annualIncome,"Annual Income");
        applicantFormLayout.addFormItem(residentialStatus,"Residential Status");
        applicantFormLayout.addFormItem(buildingNumber,"Building Number");
        applicantFormLayout.addFormItem(streetName,"Street Name");
        applicantFormLayout.addFormItem(postCode,"PostCode");
        applicantFormLayout.add(save,reset);

        add(applicantFormLayout);

        binder.bindInstanceFields(this);

        jobStatus.setItems("OTHER","PART_TIME_EMPLOYED","UNEMPLOYED","FULL_TIME_EMPLOYED","SELF_EMPLOYED","RETIRED");
        residentialStatus.setItems("OWNER","TENANT","OTHER");
        title.setItems("Mr","Mrs","Ms");

        firstName.setRequiredIndicatorVisible(true);
        lastName.setRequiredIndicatorVisible(true);
        mothersMaidenName.setRequiredIndicatorVisible(true);
        dateOfBirth.setRequiredIndicatorVisible(true);
        buildingNumber.setRequiredIndicatorVisible(true);
        postCode.setRequiredIndicatorVisible(true);
        annualIncome.setRequiredIndicatorVisible(true);

        binder.forField(firstName)
                .withValidator(new StringLengthValidator(
                        "Please add the first name", 1, null))
                .bind(Applicant::getFirstName, Applicant::setFirstName);

        binder.forField(lastName)
                .withValidator(new StringLengthValidator(
                        "Please add the last name", 1, null))
                .bind(Applicant::getLastName, Applicant::setLastName);

        binder.forField(mothersMaidenName)
                .withValidator(new StringLengthValidator(
                        "Please add the Mothers Maiden Name", 1, null))
                .bind(Applicant::getMothersMaidenName, Applicant::setMothersMaidenName);

        binder.forField(jobStatus)
                .withValidator(new StringLengthValidator(
                        "Please add the job status", 1, null))
                .bind(Applicant::getJobStatus, Applicant::setJobStatus);

        binder.forField(residentialStatus)
                .withValidator(new StringLengthValidator(
                        "Please add residential status", 1, null))
                .bind(Applicant::getResidentialStatus, Applicant::setResidentialStatus);

        SerializablePredicate<String> phoneOrEmailPredicate = value -> !phoneNumber
                .getValue().trim().isEmpty()
                || !emailAddress.getValue().trim().isEmpty();


        Binder.Binding<Applicant, String> emailBinding = binder.forField(emailAddress)
                .withValidator(phoneOrEmailPredicate,
                        "Both phone and email cannot be empty")
                .withValidator(new EmailValidator("Incorrect email address"))
                .bind(Applicant::getEmailAddress, Applicant::setEmailAddress);

        Binder.Binding<Applicant, String> phoneBinding = binder.forField(phoneNumber)
                .withValidator(phoneOrEmailPredicate,
                        "Both phone and email cannot be empty")
                .bind(Applicant::getPhoneNumber, Applicant::setPhoneNumber);


        emailAddress.addValueChangeListener(event -> phoneBinding.validate());
        phoneNumber.addValueChangeListener(event -> emailBinding.validate());
        binder.bind(dateOfBirth, Applicant::getDateOfBirth, Applicant::setDateOfBirth);
        binder.bind(annualIncome, Applicant::getAnnualIncome, Applicant::setAnnualIncome);
        binderAddress.bind(buildingNumber, Address::getBuildingNumber, Address::setBuildingNumber);
        binderAddress.bind(postCode, Address::getPostCode, Address::setPostCode);
        binderAddress.bind(streetName, Address::getStreetName, Address::setStreetName);

        save.addClickListener(event -> {
            BinderValidationStatus<Applicant> validate = binder.validate();
            logger.info("getBeanValidationErrors:"+validate.getFieldValidationErrors());
            if(validate.getFieldValidationErrors().isEmpty()){
                try {
                    //binder.writeBean(applicant);

                    Address capturedAddress = new Address();
                    capturedAddress.setBuildingNumber(buildingNumber.getValue());
                    capturedAddress.setPostCode(postCode.getValue());
                    capturedAddress.setStreetName(streetName.getValue());

                    Applicant capturedApplicant = new Applicant();
                    capturedApplicant.setTitle(title.getValue());
                    capturedApplicant.setFirstName(firstName.getValue());
                    capturedApplicant.setLastName(lastName.getValue());
                    capturedApplicant.setPhoneNumber(phoneNumber.getValue());
                    capturedApplicant.setEmailAddress(emailAddress.getValue());
                    capturedApplicant.setDateOfBirth(dateOfBirth.getValue());
                    capturedApplicant.setMothersMaidenName(mothersMaidenName.getValue());
                    capturedApplicant.setResidentialStatus(residentialStatus.getValue());
                    capturedApplicant.setJobStatus(jobStatus.getValue());
                    capturedApplicant.setJobDescription(jobDescription.getValue());
                    capturedApplicant.setAnnualIncome(annualIncome.getValue());
                    capturedApplicant.setAddress(capturedAddress);

                    save(capturedApplicant);
                    Notification.show("Applicant created successfully")
                } catch (Exception e) {
                    Notification.show("Applicant could not be created, " +
                            "please check error messages for each field.");
                    e.printStackTrace();
                }
            }
        });

        reset.addClickListener(event -> {
            // clear fields by setting null
            binder.readBean(null);
        });


    }

    public Applicant save(Applicant applicant) {
        logger.info("Inside save:" + applicant);

        Applicant createdApplicant = uiService.createApplicant(applicant);
        return createdApplicant;
    }

}