package com.amex.demo.applicantonboardingui.ui;

import com.amex.demo.applicantonboardingui.model.Applicant;
import com.amex.demo.applicantonboardingui.service.UIService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Route("list")
@PageTitle("Applicant List")
public class MainView extends VerticalLayout {

    Logger logger = LoggerFactory.getLogger(MainView.class);

    Label label = new Label("Applicant List");

    private final UIService UIService;
    private final ApplicantEditor editor;

    final TextField filterStatus;
    final Grid<Applicant> grid;

    public MainView(UIService UIService, ApplicantEditor editor) {
//        this.applicantOnboardingRepository = applicantOnboardingRepository;
        this.UIService = UIService;
        this.editor = editor;
        this.grid = new Grid<>(Applicant.class);
        this.filterStatus = new TextField();

        Button clearButton = new Button("Clear", new Icon(VaadinIcon.CLOSE_CIRCLE_O));
        clearButton.addClickListener(buttonClickEvent -> {
            filterStatus.clear();
        });

        Button logoutButton = new Button("Logout", new Icon(VaadinIcon.ARROW_RIGHT));
        logoutButton.addClickListener(event -> {
            //redirect to login page
            getUI().ifPresent(ui -> ui.navigate("login"));
        });

        grid.setHeightByRows(true);
        grid.addColumn(Applicant::getFirstName).setResizable(true);
        grid.addColumn(Applicant::getLastName).setResizable(true);
        grid.addColumn(Applicant::getMothersMaidenName).setResizable(true);
        grid.addColumn(Applicant::getDateOfBirth).setResizable(true);
        grid.addColumn(Applicant::getEmailAddress).setResizable(true);
        grid.addColumn(Applicant::getApplicantStatus).setResizable(true);
        grid.addColumn(Applicant::getJobStatus).setResizable(true);
        grid.addColumn(Applicant::getJobDescription).setResizable(true);
        grid.addColumn(Applicant::getApplicationDate).setResizable(true);
        grid.setColumns("applicantIdentifier", "firstName", "lastName","mothersMaidenName","dateOfBirth", "emailAddress", "jobStatus", "jobDescription","applicantStatus","applicationDate");
        grid.getColumnByKey("applicantIdentifier").setWidth("50px").setHeader("##").setResizable(true);

        // build layout
        HorizontalLayout layout = new HorizontalLayout();
        filterStatus.setPlaceholder("Filter by applicant status");
        layout.add(filterStatus, clearButton, logoutButton);

        HorizontalLayout gridLayout = new HorizontalLayout();
        gridLayout.add(grid);

        SplitLayout splitLayout = new SplitLayout();
        splitLayout.addToPrimary(layout, gridLayout);
        splitLayout.addToSecondary(editor);
        splitLayout.setSplitterPosition(80);
        splitLayout.setSizeFull();

        add(label, splitLayout);

        // Replace listing with filtered content when user changes filter. EAGER denotes event is fired lazily as the user is typing, when there is a small pause in the typing
        filterStatus.setValueChangeMode(ValueChangeMode.EAGER);
        filterStatus.addValueChangeListener(e -> listCustomers(e.getValue()));

        // Connect selected Customer to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> {
            editor.editCustomer(e.getValue());
        });

        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listCustomers(filterStatus.getValue());
        });
        listCustomers(null);

    }

   // tag::listCustomers[]
    void listCustomers(String filterText) {
        logger.info("Inside listCustomers::", filterText);
        if(filterText != null){
            logger.info("Inside filterText not null");
            if(filterText.equalsIgnoreCase("DECLINED") ||
               filterText.equalsIgnoreCase("APPROVED") ||
               filterText.equalsIgnoreCase("PENDING")) {
                grid.setItems(UIService.getApplicants(filterText));
            } else {
                grid.setItems(UIService.getApplicants(null));
            }
        }else{
            grid.setItems(UIService.getApplicants(null));
        }
    }
    // end::listCustomers[]

}

