package com.amex.demo.applicantonboardingui.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Route("login")
public class LoginView extends VerticalLayout {

    @Autowired
    private AuthenticationProvider authenticationProvider;

    public LoginView(){
        init();
    }

    public void init(){
        FormLayout nameLayout = new FormLayout();

        TextField username = new TextField();
        username.setLabel("user name");
        username.setPlaceholder("username");

        PasswordField passwordField = new PasswordField();
        passwordField.setLabel("password");
        passwordField.setPlaceholder("*****");

        Button loginButton = new Button("login");

        loginButton.addClickListener(event -> {
            Authentication auth = new UsernamePasswordAuthenticationToken(username.getValue(),passwordField.getValue());
            Authentication authenticated = authenticationProvider.authenticate(auth);
            SecurityContextHolder.getContext().setAuthentication(authenticated);

            //redirect to main application
            getUI().ifPresent(ui -> ui.navigate("list"));
        });

        nameLayout.add(username,passwordField, loginButton);
        add(nameLayout);
    }
}
