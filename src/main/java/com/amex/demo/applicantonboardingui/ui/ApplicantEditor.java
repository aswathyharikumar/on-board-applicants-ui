package com.amex.demo.applicantonboardingui.ui;

import com.amex.demo.applicantonboardingui.model.Applicant;
import com.amex.demo.applicantonboardingui.service.UIService;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class ApplicantEditor extends VerticalLayout implements KeyNotifier {

    private Logger logger = LoggerFactory.getLogger(ApplicantEditor.class);

    private final UIService uiService;


/**
 * The currently edited applicant
 */

    private Applicant applicant;

    TextField applicationIdentifier = new TextField("Id");
    TextField title = new TextField("Title");
    TextField firstName = new TextField("First name");
    TextField lastName = new TextField("Last name");
    TextField emailAddress = new TextField("Email address");
    TextField mothersMaidenName = new TextField("Mother's Maiden Name");
    TextField jobStatus = new TextField("Job Status");
    TextField jobDescription = new TextField("Job Description");
    TextField applicantStatus = new TextField("Applicant Status");

    TextField annualIncome = new TextField("Annual Income");
    TextField phoneNumber = new TextField("Phone Number");
    TextField residencyStatus = new TextField("Residency Status");
    TextField statusMessage = new TextField("Status Message");

    /* Action buttons */

    Button save = new Button("Save", VaadinIcon.CHECK.create());
    Button cancel = new Button("Cancel");
    HorizontalLayout actions = new HorizontalLayout(save, cancel);

    Binder<Applicant> binder = new Binder<>(Applicant.class);
    private ChangeHandler changeHandler;

    @Autowired
    public ApplicantEditor(UIService uiService) {
        this.uiService = uiService;

        add(actions, applicationIdentifier, title, firstName, lastName, emailAddress, phoneNumber, mothersMaidenName, jobStatus, jobDescription, annualIncome, applicantStatus, residencyStatus, statusMessage);
        applicationIdentifier.setVisible(false);
        firstName.setReadOnly(true);
        lastName.setReadOnly(true);
            mothersMaidenName.setReadOnly(true);

        // bind using naming convention
        binder.bindInstanceFields(this);

        // Configure and style components
        setSpacing(false);

        save.getElement().getThemeList().add("primary");

        addKeyPressListener(Key.ENTER, e -> save(applicant));

        // wire action buttons to save, delete and reset
        save.addClickListener(e -> save(applicant));
        cancel.addClickListener(e -> editCustomer(applicant));
        setVisible(false);
    }

    void save(Applicant applicant) {
        try {
            uiService.updateApplicant(applicant.getApplicantIdentifier(), applicant);
            Notification.show("Applicant updated successfully");
            changeHandler.onChange();
        }catch (Exception e){
            Notification.show("Applicant could not be updated, " +
                    "please check error messages for each field.");
        }
    }

    public interface ChangeHandler {
        void onChange();
    }

    public final void editCustomer(Applicant c) {
        if (c == null) {
            setVisible(false);
            return;
        }
        logger.info("Application Identifier is {}", c.getApplicantIdentifier());
        final boolean persisted = c.getApplicantIdentifier() > 0;
        if (persisted) {
            logger.info("Inside persisted");
            // Find fresh entity for editing
            applicant = uiService.getApplicantById(c.getApplicantIdentifier());
        }
        else {
            applicant = c;
        }
        cancel.setVisible(persisted);

        // Bind customer properties to similarly named fields
        // Could also use annotation or "manual binding" or programmatically
        // moving values from fields to entities before saving
        binder.setBean(applicant);

        setVisible(true);

        // Focus first name initially
        firstName.focus();
    }

    public void setChangeHandler(ChangeHandler h) {
        // ChangeHandler is notified when either save or delete
        // is clicked
        changeHandler = h;
    }

}
