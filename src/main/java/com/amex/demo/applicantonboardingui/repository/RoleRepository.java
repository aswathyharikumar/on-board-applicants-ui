package com.amex.demo.applicantonboardingui.repository;

import com.amex.demo.applicantonboardingui.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{

}
